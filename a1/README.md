> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

####A1 README.md file should include the following items:

* Screenshot of succesfully running Hello
* Screenshot of running http://localhost:9999
* git commands w/brief description
* Bitbucket repo links

> This is a blockquote.
>
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - creates new git repository
2. git status - information on changes to git
3. git add - adds all changes to index, preparing for git commit
4. git commit - records changes to repository
5. git push - pushes changes to remote repositories and references
6. git pull - updates current workspace with remote references and changes
7. git clone - copies an existing git repository




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:
![tomcatscreenshot.png](https://bitbucket.org/repo/7jeX6R/images/1611546185-tomcatscreenshot.png)
*Screenshot of running java Hello*:
![HelloWorld.png](https://bitbucket.org/repo/7jeX6R/images/1181199680-HelloWorld.png)
