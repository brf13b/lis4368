> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 - Advanced Web Applications Development

## Benjamin Felts

### Project 2:


1. MVC Framework
2. Client-side server validation
3. JSTL for XSS attacks
4. Crud functionality

#### README.md file should include the following items:

* Screenshot of Valid User Form Entry (customerform.jsp)
* Screenshot of Passed Validation (thanks.jsp)
* Screenshot of Display Data (modify.jsp)
* Screenshot of Modify Form (customer.jsp)
* Screenshot of Modified Data (modify.jsp)
* Screenshot of Delete Warning (modify.jsp)
* Screenshot of Database changes

> This is a blockquote.
>
> This is the second paragraph in the blockquote.

##### Assignment Screenshots:

* Valid User Form Entry (customerform.jsp)

  ![Screenshot](img/1.png)

* Passed Validation (thanks.jsp)

  ![Screenshot](img/2.png)

* Display Data (modify.jsp)

  ![Screenshot](img/3.png)

* Modify Form (customer.jsp)

  ![Screenshot](img/4.png)

* Modified Data (modify.jsp)

  ![Screenshot](img/5.png)

* Delete Warning (modify.jsp)

  ![Screenshot](img/6.png)

* Associated Database Changes (Select, Insert, Update, Delete)

  ![Screenshot](img/7.png)
