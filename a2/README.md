> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 - Advanced Web Applications Development

## Benjamin Felts

### Assignment 3 Requirements:


1. Reverse engineered database Screenshot
2. Successfully forwarded database
3. Successfully insert 10 records per entity

#### README.md file should include the following items:

1. A3.mwb
2. A3.sql
3. A3.png


> This is a blockquote.
>
> This is the second paragraph in the blockquote.

##### Assignment Screenshots:

*Screenshot of Database

![Databse Screenshot](img/A3.png)


#### Assessment Links:



*Link: querybook*
[assignment 3 mwb file link](A3.mwb)

*Link: AnotherHelloServlet*
[assignment 3 sql file link](A3.sql)
